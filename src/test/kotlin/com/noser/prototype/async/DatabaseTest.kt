package com.noser.prototype.async

import assertk.assert
import assertk.assertions.isEqualTo
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.runBlocking
import org.junit.Test


class DatabaseTest {

  @Test
  fun increment() {
    val db = Database()
    db.addPoint("Hans")
    db.addPoint("Peter")
    db.addPoint("Hans")
    assert(db.data["Peter"]?.points).isEqualTo(1)
    assert(db.data["Hans"]?.points).isEqualTo(2)
  }

  @Test
  fun incrementPoints() {
    val db = Database()
    runBlocking {
      db.addPoints("Hans", 1000)
      assert(db.data["Hans"]?.points).isEqualTo(1000)
    }
  }

  @Test
  fun incrementPointsConcurrently() {
    val db = Database()
    runBlocking {
      listOf(
        db.addPoints("Hans", 1000),
        db.addPoints("Hans", 800),
        db.addPoints("Hans", 400))
        .map { async { it } }
        .map { it.await() }
      assert(db.data["Hans"]?.points).isEqualTo(2200)
    }
  }

}
