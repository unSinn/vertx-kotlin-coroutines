package com.noser.prototype.async

import assertk.assert
import assertk.assertions.isEqualTo
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.junit.Test


class JsonTest {

  @Test
  fun serialize() {
    val mapper = jacksonObjectMapper()
    val heinz = PointsVerticle.AddPointRequest("Heinz", 100)
    val json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(heinz)
    val reincarnation = mapper.readValue<PointsVerticle.AddPointRequest>(json)
    assert(reincarnation.name).isEqualTo("Heinz")
  }


}
