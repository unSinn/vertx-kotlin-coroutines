package com.noser.prototype.async

import kotlinx.coroutines.experimental.delay
import mu.KotlinLogging

data class Player(val name: String, val points: Int)

class Database {

  private val log = KotlinLogging.logger {}

  val data = mutableMapOf<String, Player>()

  fun addPoint(name: String) {
    increment(name)
  }

  suspend fun addPoints(name: String, points: Int) {
    for (i in 1..points) {
      delay(1)
      increment(name)
    }
  }

  private fun increment(name: String) {
    val player = data.getOrPut(name) { Player(name, 0) }
    data[name] = Player(player.name, player.points + 1)
  }

}
