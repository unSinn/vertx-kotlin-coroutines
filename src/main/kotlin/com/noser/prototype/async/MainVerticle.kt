package com.noser.prototype.async

import com.fasterxml.jackson.module.kotlin.KotlinModule
import io.vertx.core.AbstractVerticle
import io.vertx.core.json.Json
import mu.KotlinLogging



class MainVerticle : AbstractVerticle() {

  private val logger = KotlinLogging.logger {}

  override fun start() {
    Json.mapper.registerModule(KotlinModule())
    println("Deploy Verticles")
    vertx.deployVerticle(WebVerticle())
    vertx.deployVerticle(PointsVerticle())
  }

}
