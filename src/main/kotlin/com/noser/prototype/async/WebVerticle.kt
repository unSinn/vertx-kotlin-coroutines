package com.noser.prototype.async

import io.vertx.core.AbstractVerticle
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.StaticHandler
import io.vertx.ext.web.handler.sockjs.SockJSHandler
import io.vertx.kotlin.ext.web.handler.sockjs.BridgeOptions
import io.vertx.kotlin.ext.web.handler.sockjs.PermittedOptions
import mu.KotlinLogging


class WebVerticle : AbstractVerticle() {

  private val log = KotlinLogging.logger {}

  override fun start() {
    println("Starting Webserver")

    val router = Router.router(vertx)

    val sockJSHandler = SockJSHandler.create(vertx)
    val options = BridgeOptions()
      .addInboundPermitted(PermittedOptions().setAddressRegex("(.*?)"))
      .addOutboundPermitted(PermittedOptions().setAddressRegex("(.*?)"))
    sockJSHandler.bridge(options)

    router.route("/eventbus/*").handler(sockJSHandler)
    router.route("/*").handler(StaticHandler.create())

    val server = vertx.createHttpServer()
      .requestHandler(router::accept)
      .listen(8080)
  }

}
