package com.noser.prototype.async

import io.vertx.core.json.Json
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.coroutines.CoroutineVerticle
import kotlinx.coroutines.experimental.launch


class PointsVerticle : CoroutineVerticle() {

  data class AddPointRequest(val name: String, val points: Int)

  val db = Database()

  override suspend fun start() {
    println("Starting PointsVerticle")
    vertx.setPeriodic(500) {
      vertx.eventBus().send("status", Json.encodePrettily(db.data.values.toList()))
    }
    vertx.eventBus().consumer<JsonObject>("add").handler { message ->
      launch {
        println("Consumer received: ${message.body()}")
        val request = message.body().mapTo(AddPointRequest::class.java)
        db.addPoints(request.name, request.points)
      }
      println("Dispatched : ${message.body()}")
    }
  }

}
