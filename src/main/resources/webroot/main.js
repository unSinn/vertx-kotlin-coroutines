var eb = new EventBus('http://localhost:8080/eventbus');

eb.onopen = function () {
  eb.registerHandler('status', function (error, message) {
    var payload = JSON.parse(message.body);
    console.log("Got: " + JSON.stringify(payload));
    var html = Mustache.to_html($("#player-list-template").html(), {"players": payload});
    $("#player-list").html(html);
  });
};


function ping() {
  console.log("Sending ping: ");
  var name = document.getElementById('name').value;
  var points = document.getElementById('points').value;
  eb.send('add', {
    name: name,
    points: points
  });
}
